var ctrlDonor = require('../controllers/donor_controller');

module.exports = function (router) {
    router
        .route('/api/donorCreate')
        .post(ctrlDonor.createDonor);
    router
        .route('/api/donorGetById/:donor_id')
        .get(ctrlDonor.getDonorById);
    router
        .route('/api/donorGetAll')
        .get(ctrlDonor.getAllDonors);
    router
        .route('/api/donorUpdate')
        .put(ctrlDonor.updateDonor);
    router
        .route('/api/donorDelete/:donor_id')
        .delete(ctrlDonor.deleteDonor);
    router
        .route('/api/user/login')
        .post(ctrlDonor.login);
    return router;
}

