import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import Home from "./component/Home";
import About from "./component/About";
import Login from "./component/Login";
import AppHome from "./component/AppHome";
import Navigation from './component/Navigation';
class App extends Component {

  componentDidMount() 
  {
    this.state={isLoggedIn: sessionStorage.getItem('token') != null && sessionStorage.getItem('token') != ""}
  }
  

  render() {
    return (
      <Router>
        <div className="App">
          <Navigation {...this.state}>  </Navigation>
          <div className="content"> 
          <Route path="/" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/login" exact component={Login} />
          <Route path="/apphome" exact component={AppHome} />
          </div>
         
        </div>
      </Router>
    );
  }
}

export default App;