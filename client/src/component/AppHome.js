
import React, { Component } from 'react';
import axios from "axios";
import DonorGrid from './DonorGrid';
import { apiURL } from '../utils/Common';
class AppHome extends Component {
  constructor(props){
    super(props);

    this.state = {
      alldonors: []
    }
  }

  componentDidMount() {
    const that = this;
    const { alldonors } = that.state;
    setTimeout(function(){
      axios.get(apiURL+'api/donorGetAll').then(result => {

        console.log(result);
        const alldonors = (result && result.data) ? result.data : [];

        that.setState({
          alldonors: alldonors
        });
        console.log(`alldonors: ${JSON.stringify(alldonors)}`);
    })});
}

render() {
  const { alldonors } = this.state;
      return (
        <div>
            <div>
              <h1>Total Donors : {alldonors.length}</h1>
            </div>
            <div>
                {
                this.state.alldonors.map(item => (
                    <DonorGrid
                        key={item.donor_id}
                        item={item}
                    />
                ))
              }
            </div>
        </div>
      );
  }
}
export default AppHome;