import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
class Navigation extends Component {
     constructor(props) {
          super(props);
          this.state = { isLoggedIn: sessionStorage.getItem('token') != null && sessionStorage.getItem('token') != "" }
          this.setState(this.state)
     }
     handleLogout = () => {
          sessionStorage.removeItem('token');
          sessionStorage.removeItem('user');
          window.location.href = '/';
     }
     render() {
          if (this.state.isLoggedIn == true) {
               return (
                    <div className="header">
                         <NavLink to="/" exact>Website Home page</NavLink>
                         <input type="button" value="Logout" onClick={this.handleLogout} />
                    </div>);
          }
          else {
               return (
                    <div className="header">
                         <NavLink to="/" exact>Website Home page</NavLink>
                         <NavLink to="/about" exact >About</NavLink>
                         <NavLink to="/login" exact>Login</NavLink>
                    </div>);
          }

     }
}
export default Navigation;